OUT_DIR=out

$(OUT_DIR):
	mkdir -p $(OUT_DIR)

BIN_NAME=resource-fd-provider

$(OUT_DIR)/$(BIN_NAME): Cargo.toml src/*.rs $(OUT_DIR)
	#cargo +nightly build -Z build-std=std,panic_abort -Z build-std-features=panic_immediate_abort --target x86_64-unknown-linux-musl --release
	cargo build --target=x86_64-unknown-linux-musl --release
	cp target/x86_64-unknown-linux-musl/release/$(BIN_NAME) $(OUT_DIR)/
	upx --best --lzma $(OUT_DIR)/$(BIN_NAME)

.PHONY: build
build: $(OUT_DIR)/$(BIN_NAME)

.PHONY: build-images
build-images: build
	docker build -t resource_fd_provider . -f docker/Dockerfile
	docker build -t io_manager_client . -f docker/Dockerfile.python

.PHONY: kind-load-images
kind-load-images: build-images
	kind load docker-image resource_fd_provider
	kind load docker-image io_manager_client

