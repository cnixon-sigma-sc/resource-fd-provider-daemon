use std::ffi::OsStr;
use std::fs::OpenOptions;
use std::io::{Read, Write};
use std::os::unix::ffi::OsStrExt;
use std::os::unix::io::AsRawFd;
use std::os::unix::net::{UnixListener, UnixStream};
use std::path::Path;
use std::thread;

use passfd::FdPassingExt;

use thiserror::Error;

use tracing::debug;

#[derive(core::fmt::Debug, Error)]
enum DaemonError {
    #[error(transparent)]
    IO(#[from] std::io::Error),
    #[error(transparent)]
    ProtocolError(#[from] ProtocolError),
    #[error(transparent)]
    Other(#[from] Box<dyn std::error::Error + Send + Sync>),
    #[error(transparent)]
    EnvVarError(#[from] std::env::VarError),
}

#[derive(core::fmt::Debug, Error)]
enum ProtocolError {
    #[error("Handshake failure, Unexpected message")]
    Handshake,
    #[error("Path too long?: {0}")]
    InvalidPathLength(usize),
    #[error("Mode too long?: {0}")]
    InvalidModeLength(usize),
}

const MAX_PATH_LENGTH: usize = 1024;

fn handle_client(mut stream: UnixStream) -> Result<(), DaemonError> {
    let mut buf: [u8; MAX_PATH_LENGTH] = [0; MAX_PATH_LENGTH];

    // handshake
    debug!("Waiting for handshake");
    stream.read_exact(&mut buf[..4])?;
    // check buf contains
    if &buf[..4] != b"INIT" {
        Err(ProtocolError::Handshake)?
    }
    debug!("Received handshake");

    stream.write_all(b"READY")?;

    loop {
        stream.read_exact(&mut buf[..5])?;
        if &buf[..5] != b"START" {
            Err(ProtocolError::Handshake)?
        }
        debug!("Waiting for path, mode pair");
        // Now we expect a path, mode pair
        let mut len_buf: [u8; 8] = [0; 8];
        stream.read_exact(&mut len_buf)?;
        let path_length = usize::from_be_bytes(len_buf);
        if path_length > MAX_PATH_LENGTH {
            Err(ProtocolError::InvalidPathLength(path_length))?
        }
        debug!("Received Path Length {}", path_length);

        stream.read_exact(&mut buf[..path_length])?;

        let path_os_str = OsStr::from_bytes(&buf[..path_length]);
        let path = Path::new(path_os_str);

        debug!("Received Path {:?}", path);

        // FIXME: Harden this up, we shouldn't have any bytes after the mode,
        // if we do we should return ProtocolError::UnexpectedBytes

        stream.read_exact(&mut len_buf)?;
        let mode_length = usize::from_be_bytes(len_buf);
        if mode_length > 4 {
            Err(ProtocolError::InvalidModeLength(mode_length))?
        }
        debug!("Received Mode Length {}", mode_length);
        let mut mode_buf: [u8; 4] = [0; 4];
        stream.read_exact(&mut mode_buf[..mode_length])?;
        let mode = &mode_buf[..];
        debug!(
            "Received Mode {:?}",
            std::str::from_utf8(&mode[..mode_length])
        );

        let mut open_options = OpenOptions::new();
        open_options.create(false);

        if mode.contains(&b'w') {
            open_options.write(true);
        };

        if mode.contains(&b'r') {
            open_options.read(true);
        };

        if mode.contains(&b'a') {
            open_options.append(true);
        };

        if mode.contains(&b'c') {
            open_options.create(true);
        };

        debug!("Opening file");

        match open_options.open(path) {
            Ok(file) => {
                debug!("Opened file");
                stream.write_all(&i32::to_be_bytes(0))?;
                debug!("Sending file descriptor");
                stream.send_fd(file.as_raw_fd())?;
            }
            Err(e) => {
                debug!("Got an Error {:?}", e);
                let mut err_msg = Vec::new();
                match e.raw_os_error() {
                    Some(n) => {
                        debug!("Raw OS Error {:?}", n);
                        stream.write_all(&i32::to_be_bytes(-1))?;
                        stream.write_all(&i32::to_be_bytes(n))?;
                        write!(&mut err_msg, "{e}")?;
                    }
                    None => {
                        debug!("Other Error");
                        stream.write_all(&i32::to_be_bytes(-2))?;
                        write!(&mut err_msg, "{e} opening {path:?}")?;
                    }
                }
                stream.write_all(&usize::to_be_bytes(err_msg.len()))?;
                stream.write_all(&err_msg)?;
            }
        }
        stream.write_all(b"DONE")?;
    }
}

fn main() -> Result<(), DaemonError> {
    tracing_subscriber::fmt()
        .compact()
        .with_max_level(tracing::Level::DEBUG)
        .init();

    let sock_path = std::path::PathBuf::from(std::env::var("FD_PROVIDER_SOCKET_PATH")?);

    debug!("FD_PROVIDER_SOCKET_PATH: {:?}", sock_path);

    if sock_path.exists() {
        debug!("Socket already exists, cleaning up");
        std::fs::remove_file(&sock_path)?;
    }

    let listener = UnixListener::bind(&sock_path)?;

    debug!("Awaiting Connections");

    // accept connections and process them, spawning a new thread for each one
    for stream in listener.incoming() {
        debug!("Connection Established");
        match stream {
            Ok(stream) => {
                /* connection succeeded */
                thread::spawn(|| {
                    handle_client(stream)
                        .map_err(|err| tracing::warn!("error handling stream: {}", err))
                });
            }
            Err(_) => {
                /* connection failed */
                break;
            }
        }
    }
    Ok(())
}
