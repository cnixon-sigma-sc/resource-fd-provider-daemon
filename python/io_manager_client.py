#!/usr/bin/env python3
# coding: utf-8

import os
from pathlib import Path
import socket


class IODaemonClient:
    def __init__(self, socket_path):
        self.socket_path = socket_path
        self.sock = socket.socket(socket.AF_UNIX)
        self.sock.connect(self.socket_path)
        self.sock.sendall(b"INIT")
        handshake_reply = self.sock.recv(5)
        assert handshake_reply == b"READY", handshake_reply

    def __del__(self):
        self.sock.close()

    def open_file(self, target_path, mode="rb"):
        # TODO: need a payload handshake
        self.sock.sendall(b"START")
        if "b" not in mode:
            raise ArgumentError("Mode must contain 'b'")
        mode_bytes = bytes(mode, "ascii")
        target_path_bytes = bytes(target_path.as_posix(), "utf-8")

        target_path_len_bytes = len(target_path_bytes).to_bytes(8, "big")
        self.sock.sendall(target_path_len_bytes)
        self.sock.sendall(target_path_bytes)

        mode_len_bytes = len(mode_bytes).to_bytes(8, "big")
        self.sock.sendall(mode_len_bytes)
        self.sock.sendall(mode_bytes)

        ret_code = int.from_bytes(self.sock.recv(4), "big", signed=True)
        if ret_code == 0:
            tup = socket.recv_fds(self.sock, 1024, 2)
            writable_fd = tup[1][0]
            ret = os.fdopen(writable_fd, mode=mode.replace("c", ""))
        else:
            system_err_code = None
            if ret_code == -1:
                system_err_code = int.from_bytes(self.sock.recv(4), "big", signed=True)
            elif ret_code == -2:
                # Some other error!
                pass
            else:
                raise Exception("ProtocolError")

            message_len = int.from_bytes(self.sock.recv(8), "big")
            message = self.sock.recv(message_len).decode("utf-8")

            if system_err_code:
                raise OSError(system_err_code, f"{message}", target_path)
            else:
                # FIXME: do this better
                raise Exception(f"{message}")

        handshake_msg = self.sock.recv(len(b"DONE"))
        assert b"DONE" == handshake_msg, handshake_msg
        return ret


if __name__ == "__main__":
    sock_path = os.getenv("FD_PROVIDER_SOCKET_PATH")
    print("FD_PROVIDER_SOCKET_PATH:", sock_path)

    data_root = Path(os.getenv("DATA_ROOT_PATH"))
    print("DATA_ROOT_PATH:", data_root)

    target_file = data_root / "test_file.txt"
    client = IODaemonClient(sock_path)
    with client.open_file(target_file, mode="wbc") as fh:
        print(f"Got fh: {fh.fileno()}")
        fh.truncate()
        fh.write(b"testing\n")
        fh.flush()

    import time

    while True:
        time.sleep(1)
